from distutils.core import setup

setup(
    name='python_payline',
    version='0.2',
    author='Pierre Pigeau',
    author_email='ppigeau@payplug.fr',
    packages=['payline'],
    url='',
    license='LICENSE.txt',
    description='',
    long_description=open('README.rst').read(),
    package_data={'payline': ['payline.wsdl']},
    install_requires=[
        "suds-jurko==0.6",
    ],
)
