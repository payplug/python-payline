# -*- coding: utf-8 -*-
import logging
import os
from payline.exceptions import APIException
from suds.client import Client
from suds.cache import ObjectCache
from time import strftime


logger = logging.getLogger('payline')


class ComplexType(object):
    def __init__(self, args):
        self.__dict__.update(args)
        del self.self

    def __str__(self):
        return str({k: v for k, v in self.__dict__.items()
                    if not k.startswith('_')})

    @property
    def soap_dict(self):
        return {v: getattr(self, k) for k, v in self._tr_params.items()}


class AlertsTrans(ComplexType):
    def __init__(self, alert_id, explanation_label, explanation_code,
            rule_name, rule_action=None, rule_criteria=None):
        super(AlertsTrans, self).__init__(locals())
        self._tr_params = {'alert_id': 'AlertId', 'explanation_label':
            'ExplanationLabel', 'explanation_code': 'ExplanationCode',
            'rule_name': 'RuleName', 'rule_action': 'RuleAction',
            'rule_criteria': 'RuleCriteria'}


class AlertsTransHist(ComplexType):
    def __init__(self, alerts_trans):
        super(AlertsTransHist, self).__init__(locals())
        self._tr_params = {'alerts_trans': 'AlertsTrans'}


class CustomerTrans(ComplexType):
    def __init__(self, is_lclf_alerted, external_transaction_id,
            reference_order, card_code, transaction_date, amount, status,
            pos_label):
        super(CustomerTrans, self).__init__(locals())
        self._tr_params = {'is_lclf_alerted': 'IsLCLFAlerted',
            'external_transaction_id': 'ExternalTransactionId',
            'reference_order': 'ReferenceOrder', 'card_code': 'CardCode',
            'transaction_date': 'TransactionDate', 'amount': 'Amount',
            'status': 'Status', 'pos_label': 'PosLabel'}


class CustomerTransHist(ComplexType):
    def __init__(self, customer_trans):
        super(CustomerTransHist, self).__init__(locals())
        self._tr_params = {'customer_trans': 'CustomerTrans'}


class PaymentMeansTrans(ComplexType):
    def __init__(self, is_lclf_alerted, external_transaction_id,
            reference_order, transaction_date, amount, status, pos_label,
            customer_data=None):
        super(PaymentMeansTrans, self).__init__(locals())
        self._tr_params = {'is_lclf_alerted': 'IsLCLFAlerted',
            'external_transaction_id': 'ExternalTransactionId',
            'reference_order': 'ReferenceOrder', 'transaction_date':
            'TransactionDate', 'amount': 'Amount', 'status': 'Status',
            'pos_label': 'PosLabel', 'customer_data': 'CustomerData'}


class PaymentMeansTransHist(ComplexType):
    def __init__(self, payment_means_trans):
        super(PaymentMeansTransHist, self).__init__(locals())
        self._tr_params = {'payment_means_trans': 'PaymentMeansTrans'}


class Address(ComplexType):
    def __init__(self, title=None, name=None, first_name=None, last_name=None,
            street1=None, street2=None, city_name=None, zip_code=None,
            country=None, phone=None, state=None):
        super(Address, self).__init__(locals())
        self._tr_params = {'title': 'title', 'name': 'name', 'first_name':
            'firstName', 'last_name': 'lastName', 'street1': 'street1',
            'street2': 'street2', 'city_name': 'cityName', 'zip_code':
            'zipCode', 'country': 'country', 'phone': 'phone', 'state':
            'state'}


class AddressInterlocutor(ComplexType):
    def __init__(self, street1=None, street2=None, city=None, zip_code=None,
            state=None, country=None):
        super(AddressInterlocutor, self).__init__(locals())
        self._tr_params = {'street1': 'street1', 'street2': 'street2', 'city':
            'city', 'zip_code': 'zipCode', 'state': 'state', 'country':
            'country'}


class AddressOwner(ComplexType):
    def __init__(self, street=None, city_name=None, zip_code=None,
            country=None, phone=None):
        super(AddressOwner, self).__init__(locals())
        self._tr_params = {'street': 'street', 'city_name': 'cityName',
            'zip_code': 'zipCode', 'country': 'country', 'phone': 'phone'}


class AssociatedTransactions(ComplexType):
    def __init__(self, transaction_id, type, date, amount, status,
            origin_transaction_id):
        super(AssociatedTransactions, self).__init__(locals())
        self._tr_params = {'transaction_id': 'transactionId', 'type': 'type',
            'date': 'date', 'amount': 'amount', 'status': 'status',
            'origin_transaction_id': 'originTransactionId'}


class AssociatedTransactionsList(ComplexType):
    def __init__(self, associated_transactions):
        super(AssociatedTransactionsList, self).__init__(locals())
        self._tr_params = {'associated_transactions': 'associatedTransactions'}


class Authentication3DSecure(ComplexType):
    def __init__(self, md=None, pares=None, xid=None, eci=None, cavv=None,
            cavv_algorithm=None, vads_result=None, type_securisation=None):
        super(Authentication3DSecure, self).__init__(locals())
        self._tr_params = {'md': 'md', 'pares': 'pares', 'xid': 'xid', 'eci':
            'eci', 'cavv': 'cavv', 'cavv_algorithm': 'cavvAlgorithm',
            'vads_result': 'vadsResult', 'type_securisation':
            'typeSecurisation'}


class Authorization(ComplexType):
    def __init__(self, number, date):
        super(Authorization, self).__init__(locals())
        self._tr_params = {'number': 'number', 'date': 'date'}


class BankAccount(ComplexType):
    def __init__(self, bank_code=None, iban=None, rib=None):
        super(BankAccount, self).__init__(locals())
        self._tr_params = {'bank_code': 'bankCode', 'iban': 'iban', 'rib':
            'rib'}


class BankAccountData(ComplexType):
    def __init__(self, country_code=None, bank_code=None, account_number=None,
            key=None):
        super(BankAccountData, self).__init__(locals())
        self._tr_params = {'country_code': 'countryCode', 'bank_code':
            'bankCode', 'account_number': 'accountNumber', 'key': 'key'}


class BillingRecord(ComplexType):
    def __init__(self, date, amount, status, result=None, transaction=None,
            authorization=None):
        super(BillingRecord, self).__init__(locals())
        self._tr_params = {'date': 'date', 'amount': 'amount', 'status':
            'status', 'result': 'result', 'transaction': 'transaction',
            'authorization': 'authorization'}


class BillingRecordList(ComplexType):
    def __init__(self, billing_record):
        super(BillingRecordList, self).__init__(locals())
        self._tr_params = {'billing_record': 'billingRecord'}


class Buyer(ComplexType):
    def __init__(self, title=None, last_name=None, first_name=None, email=None,
            shipping_adress=None, billing_address=None,
            account_create_date=None, account_average_amount=None,
            account_order_count=None, wallet_id=None, wallet_displayed=None,
            wallet_secured=None, wallet_card_ind=None, ip=None,
            mobile_phone=None, customer_id=None):
        super(Buyer, self).__init__(locals())
        self._tr_params = {'title': 'title', 'last_name': 'lastName',
            'first_name': 'firstName', 'email': 'email', 'shipping_adress':
            'shippingAdress', 'billing_address': 'billingAddress',
            'account_create_date': 'accountCreateDate',
            'account_average_amount': 'accountAverageAmount',
            'account_order_count': 'accountOrderCount', 'wallet_id':
            'walletId', 'wallet_displayed': 'walletDisplayed',
            'wallet_secured': 'walletSecured', 'wallet_card_ind':
            'walletCardInd', 'ip': 'ip', 'mobile_phone': 'mobilePhone',
            'customer_id': 'customerId'}


class Capture(ComplexType):
    def __init__(self, transaction_id, payment):
        super(Capture, self).__init__(locals())
        self._tr_params = {'transaction_id': 'transactionID', 'payment':
            'payment'}


class CaptureAuthorizationList(ComplexType):
    def __init__(self, capture):
        super(CaptureAuthorizationList, self).__init__(locals())
        self._tr_params = {'capture': 'capture'}


class Card(ComplexType):
    def __init__(self, type, encryption_key_id=None, encrypted_data=None,
            number=None, expiration_date=None, cvx=None,
            owner_birthday_date=None, password=None, card_present=None,
            cardholder=None, token=None):
        super(Card, self).__init__(locals())
        self._tr_params = {'type': 'type', 'encryption_key_id':
            'encryptionKeyId', 'encrypted_data': 'encryptedData', 'number':
            'number', 'expiration_date': 'expirationDate', 'cvx': 'cvx',
            'owner_birthday_date': 'ownerBirthdayDate', 'password': 'password',
            'card_present': 'cardPresent', 'cardholder': 'cardholder', 'token':
            'token'}


class CardOut(ComplexType):
    def __init__(self, number, type, expiration_date, cardholder, token):
        super(CardOut, self).__init__(locals())
        self._tr_params = {'number': 'number', 'type': 'type',
            'expiration_date': 'expirationDate', 'cardholder': 'cardholder',
            'token': 'token'}


class Cards(ComplexType):
    def __init__(self, wallet_id, card, last_name=None, first_name=None,
            email=None, shipping_address=None, card_ind=None, comment=None,
            is_disabled=None, disable_date=None, disable_status=None,
            extended_card=None, default=None):
        super(Cards, self).__init__(locals())
        self._tr_params = {'wallet_id': 'walletId', 'card': 'card',
            'last_name': 'lastName', 'first_name': 'firstName', 'email':
            'email', 'shipping_address': 'shippingAddress', 'card_ind':
            'cardInd', 'comment': 'comment', 'is_disabled': 'isDisabled',
            'disable_date': 'disableDate', 'disable_status': 'disableStatus',
            'extended_card': 'extendedCard', 'default': 'default'}


class CardsList(ComplexType):
    def __init__(self, cards):
        super(CardsList, self).__init__(locals())
        self._tr_params = {'cards': 'cards'}


class Cheque(ComplexType):
    def __init__(self, number):
        super(Cheque, self).__init__(locals())
        self._tr_params = {'number': 'number'}


class ConnectionData(ComplexType):
    def __init__(self, merchant_id, user_id, password, secret_question,
            secret_answer):
        super(ConnectionData, self).__init__(locals())
        self._tr_params = {'merchant_id': 'merchantId', 'user_id': 'userId',
            'password': 'password', 'secret_question': 'secretQuestion',
            'secret_answer': 'secretAnswer'}


class Contract(ComplexType):
    def __init__(self, settlement_type, logo_enable, small_logo_mime,
            small_logo, normal_logo_mime, normal_logo, contribution,
            card_type=None, label=None, contract_number=None, currency=None,
            max_amount_per_transaction=None, technical_data=None,
            bank_account=None, acquirer_interlocutor=None, description=None):
        super(Contract, self).__init__(locals())
        self._tr_params = {'settlement_type': 'settlementType', 'logo_enable':
            'logoEnable', 'small_logo_mime': 'smallLogoMime', 'small_logo':
            'smallLogo', 'normal_logo_mime': 'normalLogoMime', 'normal_logo':
            'normalLogo', 'contribution': 'contribution', 'card_type':
            'cardType', 'label': 'label', 'contract_number': 'contractNumber',
            'currency': 'currency', 'max_amount_per_transaction':
            'maxAmountPerTransaction', 'technical_data': 'technicalData',
            'bank_account': 'bankAccount', 'acquirer_interlocutor':
            'acquirerInterlocutor', 'description': 'description'}


class ContractNumberWalletList(ComplexType):
    def __init__(self, contract_number_wallet):
        super(ContractNumberWalletList, self).__init__(locals())
        self._tr_params = {'contract_number_wallet': 'contractNumberWallet'}


class Contribution(ComplexType):
    def __init__(self, enable, type=None, value=None, nb_free_transaction=None,
            min_amount_transaction=None, max_amount_transaction=None):
        super(Contribution, self).__init__(locals())
        self._tr_params = {'enable': 'enable', 'type': 'type', 'value':
            'value', 'nb_free_transaction': 'nbFreeTransaction',
            'min_amount_transaction': 'minAmountTransaction',
            'max_amount_transaction': 'maxAmountTransaction'}


class CustomPaymentPageCode(ComplexType):
    def __init__(self, code=None, label=None, type=None):
        super(CustomPaymentPageCode, self).__init__(locals())
        self._tr_params = {'code': 'code', 'label': 'label', 'type': 'type'}


class Details(ComplexType):
    def __init__(self, details):
        super(Details, self).__init__(locals())
        self._tr_params = {'details': 'details'}


class ExtendedCardType(ComplexType):
    def __init__(self, country=None, is_cvd=None, bank=None, type=None,
            network=None, product=None):
        super(ExtendedCardType, self).__init__(locals())
        self._tr_params = {'country': 'country', 'is_cvd': 'isCvd', 'bank':
            'bank', 'type': 'type', 'network': 'network', 'product': 'product'}


class FailedListObject(ComplexType):
    def __init__(self, failed_object):
        super(FailedListObject, self).__init__(locals())
        self._tr_params = {'failed_object': 'failedObject'}


class FailedObject(ComplexType):
    def __init__(self, transaction_id, result):
        super(FailedObject, self).__init__(locals())
        self._tr_params = {'transaction_id': 'transactionID', 'result':
            'result'}


class FraudResultDetails(ComplexType):
    def __init__(self, code, short_message, long_message):
        super(FraudResultDetails, self).__init__(locals())
        self._tr_params = {'code': 'code', 'short_message': 'shortMessage',
            'long_message': 'longMessage'}


class Iban(ComplexType):
    def __init__(self, country_code=None, check_key=None, bban=None, bic=None):
        super(Iban, self).__init__(locals())
        self._tr_params = {'country_code': 'CountryCode', 'check_key':
            'checkKey', 'bban': 'BBAN', 'bic': 'BIC'}


class Interlocutor(ComplexType):
    def __init__(self, first_name=None, last_name=None, email=None, phone=None,
            mobile=None, fax=None, address_interlocutor=None):
        super(Interlocutor, self).__init__(locals())
        self._tr_params = {'first_name': 'firstName', 'last_name': 'lastName',
            'email': 'email', 'phone': 'phone', 'mobile': 'mobile', 'fax':
            'fax', 'address_interlocutor': 'addressInterlocutor'}


class Key(ComplexType):
    def __init__(self, key_id, modulus, public_exponent, expiration_date):
        super(Key, self).__init__(locals())
        self._tr_params = {'key_id': 'keyId', 'modulus': 'modulus',
            'public_exponent': 'publicExponent', 'expiration_date':
            'expirationDate'}


class Option(ComplexType):
    def __init__(self, id, subscribed=None, end_date=None):
        super(Option, self).__init__(locals())
        self._tr_params = {'id': 'id', 'subscribed': 'subscribed', 'end_date':
            'endDate'}


class Order(ComplexType):
    def __init__(self, ref, amount, currency, origin=None, country=None,
            taxes=None, date=None, details=None, delivery_time=None,
            delivery_mode=None, delivery_expected_date=None,
            delivery_expected_delay=None):
        if date is None:
            date = strftime('%d/%m/%Y %H:%M')
        super(Order, self).__init__(locals())
        self._tr_params = {'ref': 'ref', 'amount': 'amount', 'currency':
            'currency', 'origin': 'origin', 'country': 'country', 'taxes':
            'taxes', 'date': 'date', 'details': 'details', 'delivery_time':
            'deliveryTime', 'delivery_mode': 'deliveryMode',
            'delivery_expected_date': 'deliveryExpectedDate',
            'delivery_expected_delay': 'deliveryExpectedDelay'}


class OrderDetail(ComplexType):
    def __init__(self, ref=None, price=None, quantity=None, comment=None,
            category=None, brand=None):
        super(OrderDetail, self).__init__(locals())
        self._tr_params = {'ref': 'ref', 'price': 'price', 'quantity':
            'quantity', 'comment': 'comment', 'category': 'category', 'brand':
            'brand'}


class Owner(ComplexType):
    def __init__(self, last_name=None, first_name=None, billing_address=None,
            issue_card_date=None):
        super(Owner, self).__init__(locals())
        self._tr_params = {'last_name': 'lastName', 'first_name': 'firstName',
            'billing_address': 'billingAddress', 'issue_card_date':
            'issueCardDate'}


class Payment(ComplexType):
    def __init__(self, amount, currency, action, mode, contract_number,
            differed_action_date=None):
        super(Payment, self).__init__(locals())
        self._tr_params = {'amount': 'amount', 'currency': 'currency',
            'action': 'action', 'mode': 'mode', 'contract_number':
            'contractNumber', 'differed_action_date': 'differedActionDate'}


class PaymentAdditional(ComplexType):
    def __init__(self, transaction, payment, authorization,
            authentication3_d_secure=None, card=None, extended_card=None):
        super(PaymentAdditional, self).__init__(locals())
        self._tr_params = {'transaction': 'transaction', 'payment': 'payment',
            'authorization': 'authorization', 'authentication3_d_secure':
            'authentication3DSecure', 'card': 'card', 'extended_card':
            'extendedCard'}


class PaymentAdditionalList(ComplexType):
    def __init__(self, payment_additional):
        super(PaymentAdditionalList, self).__init__(locals())
        self._tr_params = {'payment_additional': 'paymentAdditional'}


class PointOfSell(ComplexType):
    def __init__(self, contracts, custom_payment_page_code_list, siret=None,
            code_mcc=None, label=None, webmaster_email=None, comments=None,
            webstore_url=None, notification_url=None, private_life_url=None,
            sale_cond_url=None, buyer_must_accept_sale_cond=None,
            end_of_payment_redirection=None, ticket_send=None,
            virtual_terminal=None):
        super(PointOfSell, self).__init__(locals())
        self._tr_params = {'contracts': 'contracts',
            'custom_payment_page_code_list': 'customPaymentPageCodeList',
            'siret': 'siret', 'code_mcc': 'codeMcc', 'label': 'label',
            'webmaster_email': 'webmasterEmail', 'comments': 'comments',
            'webstore_url': 'webstoreURL', 'notification_url':
            'notificationURL', 'private_life_url': 'privateLifeURL',
            'sale_cond_url': 'saleCondURL', 'buyer_must_accept_sale_cond':
            'buyerMustAcceptSaleCond', 'end_of_payment_redirection':
            'endOfPaymentRedirection', 'ticket_send': 'ticketSend',
            'virtual_terminal': 'virtualTerminal'}


class PrivateData(ComplexType):
    def __init__(self, key, value):
        super(PrivateData, self).__init__(locals())
        self._tr_params = {'key': 'key', 'value': 'value'}


class PrivateDataList(ComplexType):
    def __init__(self, private_data):
        super(PrivateDataList, self).__init__(locals())
        self._tr_params = {'private_data': 'privateData'}


class Recurring(ComplexType):
    def __init__(self, amount, billing_cycle, billing_left, first_amount=None,
            billing_day=None, start_date=None):
        super(Recurring, self).__init__(locals())
        self._tr_params = {'amount': 'amount', 'billing_cycle': 'billingCycle',
            'billing_left': 'billingLeft', 'first_amount': 'firstAmount',
            'billing_day': 'billingDay', 'start_date': 'startDate'}


class Refund(ComplexType):
    def __init__(self, transaction_id, payment):
        super(Refund, self).__init__(locals())
        self._tr_params = {'transaction_id': 'transactionID', 'payment':
            'payment'}


class RefundAuthorizationList(ComplexType):
    def __init__(self, refund):
        super(RefundAuthorizationList, self).__init__(locals())
        self._tr_params = {'refund': 'refund'}


class ResetAuthorizationList(ComplexType):
    def __init__(self, transaction_id):
        super(ResetAuthorizationList, self).__init__(locals())
        self._tr_params = {'transaction_id': 'transactionID'}


class Result(ComplexType):
    def __init__(self, code, short_message=None, long_message=None):
        super(Result, self).__init__(locals())
        self._tr_params = {'code': 'code', 'short_message': 'shortMessage',
            'long_message': 'longMessage'}


class Rib(ComplexType):
    def __init__(self, teller_code=None, account_number=None, key=None):
        super(Rib, self).__init__(locals())
        self._tr_params = {'teller_code': 'tellerCode', 'account_number':
            'accountNumber', 'key': 'key'}


class ScoringCheque(ComplexType):
    def __init__(self, cheque_number, additional_data_response, terminal_id,
            additional_private_data):
        super(ScoringCheque, self).__init__(locals())
        self._tr_params = {'cheque_number': 'chequeNumber',
            'additional_data_response': 'additionalDataResponse',
            'terminal_id': 'terminalId', 'additional_private_data':
            'additionalPrivateData'}


class SelectedContractList(ComplexType):
    def __init__(self, selected_contract):
        super(SelectedContractList, self).__init__(locals())
        self._tr_params = {'selected_contract': 'selectedContract'}


class StatusHistory(ComplexType):
    def __init__(self, transaction_id, date, amount, fees, status,
            origin_transaction_id):
        super(StatusHistory, self).__init__(locals())
        self._tr_params = {'transaction_id': 'transactionId', 'date': 'date',
            'amount': 'amount', 'fees': 'fees', 'status': 'status',
            'origin_transaction_id': 'originTransactionId'}


class StatusHistoryList(ComplexType):
    def __init__(self, status_history):
        super(StatusHistoryList, self).__init__(locals())
        self._tr_params = {'status_history': 'statusHistory'}


class Subscription(ComplexType):
    def __init__(self, id, option):
        super(Subscription, self).__init__(locals())
        self._tr_params = {'id': 'id', 'option': 'option'}


class TechnicalData(ComplexType):
    def __init__(self, terminal_number=None, gt_instance=None,
            payment_profil=None):
        super(TechnicalData, self).__init__(locals())
        self._tr_params = {'terminal_number': 'terminalNumber', 'gt_instance':
            'GTInstance', 'payment_profil': 'paymentProfil'}


class TicketSend(ComplexType):
    def __init__(self, to_buyer=None, to_merchant=None):
        super(TicketSend, self).__init__(locals())
        self._tr_params = {'to_buyer': 'toBuyer', 'to_merchant': 'toMerchant'}


class Transaction(ComplexType):
    def __init__(self, id, date, is_possible_fraud, is_duplicated=None,
            fraud_result=None, fraud_result_details=None, explanation=None,
            three_d_secure=None, score=None, external_wallet_type=None,
            external_wallet_contract_number=None):
        super(Transaction, self).__init__(locals())
        self._tr_params = {'id': 'id', 'date': 'date', 'is_possible_fraud':
            'isPossibleFraud', 'is_duplicated': 'isDuplicated', 'fraud_result':
            'fraudResult', 'fraud_result_details': 'fraudResultDetails',
            'explanation': 'explanation', 'three_d_secure': 'threeDSecure',
            'score': 'score', 'external_wallet_type': 'externalWalletType',
            'external_wallet_contract_number': 'externalWalletContractNumber'}


class TransactionList(ComplexType):
    def __init__(self, transaction):
        super(TransactionList, self).__init__(locals())
        self._tr_params = {'transaction': 'transaction'}


class VirtualTerminal(ComplexType):
    def __init__(self, label, inactivity_delay, logo, functions):
        super(VirtualTerminal, self).__init__(locals())
        self._tr_params = {'label': 'label', 'inactivity_delay':
            'inactivityDelay', 'logo': 'logo', 'functions': 'functions'}


class VirtualTerminalFunction(ComplexType):
    def __init__(self, function, label, function_parameter):
        super(VirtualTerminalFunction, self).__init__(locals())
        self._tr_params = {'function': 'function', 'label': 'label',
            'function_parameter': 'functionParameter'}


class Wallet(ComplexType):
    def __init__(self, wallet_id, card, last_name=None, first_name=None,
            email=None, shipping_address=None, comment=None, default=None):
        super(Wallet, self).__init__(locals())
        self._tr_params = {'wallet_id': 'walletId', 'card': 'card',
            'last_name': 'lastName', 'first_name': 'firstName', 'email':
            'email', 'shipping_address': 'shippingAddress', 'comment':
            'comment', 'default': 'default'}


class WalletIdList(ComplexType):
    def __init__(self, wallet_id):
        super(WalletIdList, self).__init__(locals())
        self._tr_params = {'wallet_id': 'walletId'}


class Payline(object):
    SANDBOX_LOCATION = 'https://homologation.payline.com/V4/services/'
    PRODUCTION_LOCATION = 'https://services.payline.com/V4/services/'
    WSDL_URL = ('file://' + os.path.dirname(os.path.realpath(__file__))
                + '/payline.wsdl')

    def __init__(self, merchant_id, access_key, production=False):
        self.merchant_id = merchant_id
        self.access_key = access_key
        if not production:
            self._location = self.SANDBOX_LOCATION
        else:
            self._location = self.PRODUCTION_LOCATION
        cache_path = os.path.dirname(os.path.realpath(__file__)) + '/suds_cache'
        cache = ObjectCache(cache_path, days=90)
        self._client = Client(self.WSDL_URL, cachingpolicy=1,
            username=self.merchant_id, password=self.access_key,
            cache=cache)

    def ws_request(self, method, api_name, **params):
        self._client.set_options(location=self._location + api_name)
        logger.info('Calling %s method with params: %s' % (method, params))
        try:
            answer = getattr(self._client.service[api_name][api_name], method)(**params)
        except Exception as e:
            raise APIException(e.message)
        #if hasattr(answer, 'result') and answer.result.code != '00000':
        #    logger.error('Error while calling %s method with params: %s' % (method, params))
        #    raise APIException(answer)
        return answer

    def soap_dict(self, complex_type):
        return complex_type.soap_dict if complex_type else None

    def create_web_wallet(self, contract_number, buyer, return_url, cancel_url,
            selected_contract_list=None, update_personal_details=None,
            owner=None, language_code=None, custom_payment_page_code=None,
            security_mode=None, notification_url=None, private_data_list=None,
            custom_payment_template_url=None,
            contract_number_wallet_list=None):
        """
        :type contract_number: String
        :type buyer: Buyer
        :type return_url: String
        :type cancel_url: String
        :type selected_contract_list: SelectedContractList
        :type update_personal_details: String
        :type owner: Owner
        :type language_code: String
        :type custom_payment_page_code: String
        :type security_mode: String
        :type notification_url: String
        :type private_data_list: PrivateDataList
        :type custom_payment_template_url: String
        :type contract_number_wallet_list: ContractNumberWalletList
        """
        buyer = self.soap_dict(buyer)
        selected_contract_list = self.soap_dict(selected_contract_list)
        owner = self.soap_dict(owner)
        private_data_list = self.soap_dict(private_data_list)
        contract_number_wallet_list = self.soap_dict(contract_number_wallet_list)
        return self.ws_request('createWebWallet', 'WebPaymentAPI',
            contractNumber=contract_number, buyer=buyer, returnURL=return_url,
            cancelURL=cancel_url, selectedContractList=selected_contract_list,
            updatePersonalDetails=update_personal_details, owner=owner,
            languageCode=language_code,
            customPaymentPageCode=custom_payment_page_code,
            securityMode=security_mode, notificationURL=notification_url,
            privateDataList=private_data_list,
            customPaymentTemplateURL=custom_payment_template_url,
            contractNumberWalletList=contract_number_wallet_list, version=3)

    def do_web_payment(self, payment, return_url, cancel_url, order,
            notification_url=None, selected_contract_list=None,
            second_selected_contract_list=None, private_data_list=None,
            language_code=None, custom_payment_page_code=None, buyer=None,
            owner=None, security_mode=None, recurring=None,
            custom_payment_template_url=None,
            contract_number_wallet_list=None):
        """
        :type payment: Payment
        :type return_url: String
        :type cancel_url: String
        :type order: Order
        :type notification_url: String
        :type selected_contract_list: SelectedContractList
        :type second_selected_contract_list: SelectedContractList
        :type private_data_list: PrivateDataList
        :type language_code: String
        :type custom_payment_page_code: String
        :type buyer: Buyer
        :type owner: Owner
        :type security_mode: String
        :type recurring: Recurring
        :type custom_payment_template_url: String
        :type contract_number_wallet_list: ContractNumberWalletList
        """
        payment = self.soap_dict(payment)
        order = self.soap_dict(order)
        selected_contract_list = self.soap_dict(selected_contract_list)
        second_selected_contract_list = self.soap_dict(second_selected_contract_list)
        private_data_list = self.soap_dict(private_data_list)
        buyer = self.soap_dict(buyer)
        owner = self.soap_dict(owner)
        recurring = self.soap_dict(recurring)
        contract_number_wallet_list = self.soap_dict(contract_number_wallet_list)
        return self.ws_request('doWebPayment', 'WebPaymentAPI',
            payment=payment, returnURL=return_url, cancelURL=cancel_url,
            order=order, notificationURL=notification_url,
            selectedContractList=selected_contract_list,
            secondSelectedContractList=second_selected_contract_list,
            privateDataList=private_data_list, languageCode=language_code,
            customPaymentPageCode=custom_payment_page_code, buyer=buyer,
            owner=owner, securityMode=security_mode, recurring=recurring,
            customPaymentTemplateURL=custom_payment_template_url,
            contractNumberWalletList=contract_number_wallet_list, version=3)

    def get_web_payment_details(self, token):
        """
        :type token: String
        """
        return self.ws_request('getWebPaymentDetails', 'WebPaymentAPI',
            token=token, version=3)

    def get_web_wallet(self, token):
        """
        :type token: String
        """
        return self.ws_request('getWebWallet', 'WebPaymentAPI', token=token,
            version=3)

    def manage_web_wallet(self, contract_number, buyer, return_url, cancel_url,
            selected_contract_list=None, update_personal_details=None,
            owner=None, language_code=None, custom_payment_page_code=None,
            security_mode=None, notification_url=None, private_data_list=None,
            custom_payment_template_url=None,
            contract_number_wallet_list=None):
        """
        :type contract_number: String
        :type buyer: Buyer
        :type return_url: String
        :type cancel_url: String
        :type selected_contract_list: SelectedContractList
        :type update_personal_details: String
        :type owner: Owner
        :type language_code: String
        :type custom_payment_page_code: String
        :type security_mode: String
        :type notification_url: String
        :type private_data_list: PrivateDataList
        :type custom_payment_template_url: String
        :type contract_number_wallet_list: ContractNumberWalletList
        """
        buyer = self.soap_dict(buyer)
        selected_contract_list = self.soap_dict(selected_contract_list)
        owner = self.soap_dict(owner)
        private_data_list = self.soap_dict(private_data_list)
        contract_number_wallet_list = self.soap_dict(contract_number_wallet_list)
        return self.ws_request('manageWebWallet', 'WebPaymentAPI',
            contractNumber=contract_number, buyer=buyer, returnURL=return_url,
            cancelURL=cancel_url, selectedContractList=selected_contract_list,
            updatePersonalDetails=update_personal_details, owner=owner,
            languageCode=language_code,
            customPaymentPageCode=custom_payment_page_code,
            securityMode=security_mode, notificationURL=notification_url,
            privateDataList=private_data_list,
            customPaymentTemplateURL=custom_payment_template_url,
            contractNumberWalletList=contract_number_wallet_list, version=3)

    def update_web_wallet(self, contract_number, wallet_id, return_url,
            cancel_url, card_ind=None, update_personal_details=None,
            update_owner_details=None, update_payment_details=None, buyer=None,
            language_code=None, custom_payment_page_code=None,
            security_mode=None, notification_url=None, private_data_list=None,
            custom_payment_template_url=None,
            contract_number_wallet_list=None):
        """
        :type contract_number: String
        :type wallet_id: String
        :type return_url: String
        :type cancel_url: String
        :type card_ind: String
        :type update_personal_details: String
        :type update_owner_details: String
        :type update_payment_details: String
        :type buyer: Buyer
        :type language_code: String
        :type custom_payment_page_code: String
        :type security_mode: String
        :type notification_url: String
        :type private_data_list: PrivateDataList
        :type custom_payment_template_url: String
        :type contract_number_wallet_list: ContractNumberWalletList
        """
        buyer = self.soap_dict(buyer)
        private_data_list = self.soap_dict(private_data_list)
        contract_number_wallet_list = self.soap_dict(contract_number_wallet_list)
        return self.ws_request('updateWebWallet', 'WebPaymentAPI',
            contractNumber=contract_number, walletId=wallet_id,
            returnURL=return_url, cancelURL=cancel_url, cardInd=card_ind,
            updatePersonalDetails=update_personal_details,
            updateOwnerDetails=update_owner_details,
            updatePaymentDetails=update_payment_details, buyer=buyer,
            languageCode=language_code,
            customPaymentPageCode=custom_payment_page_code,
            securityMode=security_mode, notificationURL=notification_url,
            privateDataList=private_data_list,
            customPaymentTemplateURL=custom_payment_template_url,
            contractNumberWalletList=contract_number_wallet_list, version=3)

    def get_alert_details(self, alert_id, transaction_id):
        """
        :type alert_id: String
        :type transaction_id: String
        """
        return self.ws_request('getAlertDetails', 'ExtendedAPI',
            AlertId=alert_id, TransactionId=transaction_id)

    def get_transaction_details(self, transaction_id=None, order_ref=None,
            start_date=None, end_date=None, transaction_history=None,
            archive_search=None):
        """
        :type transaction_id: String
        :type order_ref: String
        :type start_date: String
        :type end_date: String
        :type transaction_history: String
        :type archive_search: String
        """
        return self.ws_request('getTransactionDetails', 'ExtendedAPI',
            transactionId=transaction_id, orderRef=order_ref,
            startDate=start_date, endDate=end_date,
            transactionHistory=transaction_history,
            archiveSearch=archive_search, version=3)

    def transactions_search(self, transaction_id=None, order_ref=None,
            start_date=None, end_date=None, contract_number=None,
            authorization_number=None, return_code=None, payment_mean=None,
            transaction_type=None, name=None, first_name=None, email=None,
            card_number=None, currency=None, min_amount=None, max_amount=None,
            wallet_id=None, sequence_number=None, token=None):
        """
        :type transaction_id: String
        :type order_ref: String
        :type start_date: String
        :type end_date: String
        :type contract_number: String
        :type authorization_number: String
        :type return_code: String
        :type payment_mean: String
        :type transaction_type: String
        :type name: String
        :type first_name: String
        :type email: String
        :type card_number: String
        :type currency: String
        :type min_amount: String
        :type max_amount: String
        :type wallet_id: String
        :type sequence_number: String
        :type token: String
        """
        return self.ws_request('transactionsSearch', 'ExtendedAPI',
            transactionId=transaction_id, orderRef=order_ref,
            startDate=start_date, endDate=end_date,
            contractNumber=contract_number,
            authorizationNumber=authorization_number, returnCode=return_code,
            paymentMean=payment_mean, transactionType=transaction_type,
            name=name, firstName=first_name, email=email,
            cardNumber=card_number, currency=currency, minAmount=min_amount,
            maxAmount=max_amount, walletId=wallet_id,
            sequenceNumber=sequence_number, token=token, version=3)

    def create_merchant(self, currency, corporate_name=None, public_name=None,
            national_id=None, distributor=None, merchant_address=None,
            business_interlocutor=None, technical_interlocutor=None,
            subscription=None, poss=None, partner=None):
        """
        :type currency: UNKNOWN
        :type corporate_name: String
        :type public_name: String
        :type national_id: UNKNOWN
        :type distributor: String
        :type merchant_address: AddressInterlocutor
        :type business_interlocutor: Interlocutor
        :type technical_interlocutor: Interlocutor
        :type subscription: Subscription
        :type poss: UNKNOWN
        :type partner: String
        """
        merchant_address = self.soap_dict(merchant_address)
        business_interlocutor = self.soap_dict(business_interlocutor)
        technical_interlocutor = self.soap_dict(technical_interlocutor)
        subscription = self.soap_dict(subscription)
        return self.ws_request('createMerchant', 'DirectPaymentAPI',
            currency=currency, corporateName=corporate_name,
            publicName=public_name, nationalID=national_id,
            distributor=distributor, merchantAddress=merchant_address,
            businessInterlocutor=business_interlocutor,
            technicalInterlocutor=technical_interlocutor,
            subscription=subscription, poss=poss, partner=partner)

    def create_wallet(self, contract_number, wallet, buyer=None, owner=None,
            private_data_list=None, authentication3_d_secure=None, media=None,
            contract_number_wallet_list=None):
        """
        :type contract_number: String
        :type wallet: Wallet
        :type buyer: Buyer
        :type owner: Owner
        :type private_data_list: PrivateDataList
        :type authentication3_d_secure: Authentication3DSecure
        :type media: String
        :type contract_number_wallet_list: ContractNumberWalletList
        """
        wallet = self.soap_dict(wallet)
        buyer = self.soap_dict(buyer)
        owner = self.soap_dict(owner)
        private_data_list = self.soap_dict(private_data_list)
        authentication3_d_secure = self.soap_dict(authentication3_d_secure)
        contract_number_wallet_list = self.soap_dict(contract_number_wallet_list)
        return self.ws_request('createWallet', 'DirectPaymentAPI',
            contractNumber=contract_number, wallet=wallet, buyer=buyer,
            owner=owner, privateDataList=private_data_list,
            authentication3DSecure=authentication3_d_secure, media=media,
            contractNumberWalletList=contract_number_wallet_list, version=3)

    def disable_payment_record(self, contract_number, payment_record_id):
        """
        :type contract_number: String
        :type payment_record_id: String
        """
        return self.ws_request('disablePaymentRecord', 'DirectPaymentAPI',
            contractNumber=contract_number, paymentRecordId=payment_record_id)

    def disable_wallet(self, contract_number, wallet_id_list, card_ind=None):
        """
        :type contract_number: String
        :type wallet_id_list: WalletIdList
        :type card_ind: String
        """
        wallet_id_list = self.soap_dict(wallet_id_list)
        return self.ws_request('disableWallet', 'DirectPaymentAPI',
            contractNumber=contract_number, walletIdList=wallet_id_list,
            cardInd=card_ind)

    def do_authorization(self, payment, bank_account_data, card, order,
            buyer=None, owner=None, private_data_list=None,
            authentication3_d_secure=None, media=None):
        """
        :type payment: Payment
        :type bank_account_data: BankAccountData
        :type card: Card
        :type order: Order
        :type buyer: Buyer
        :type owner: Owner
        :type private_data_list: PrivateDataList
        :type authentication3_d_secure: Authentication3DSecure
        :type media: String
        """
        payment = self.soap_dict(payment)
        bank_account_data = self.soap_dict(bank_account_data)
        card = self.soap_dict(card)
        order = self.soap_dict(order)
        buyer = self.soap_dict(buyer)
        owner = self.soap_dict(owner)
        private_data_list = self.soap_dict(private_data_list)
        authentication3_d_secure = self.soap_dict(authentication3_d_secure)
        return self.ws_request('doAuthorization', 'DirectPaymentAPI',
            payment=payment, bankAccountData=bank_account_data, card=card,
            order=order, buyer=buyer, owner=owner,
            privateDataList=private_data_list,
            authentication3DSecure=authentication3_d_secure, media=media,
            version=3)

    def do_capture(self, transaction_id, payment, private_data_list=None,
            sequence_number=None, media=None):
        """
        :type transaction_id: String
        :type payment: Payment
        :type private_data_list: PrivateDataList
        :type sequence_number: String
        :type media: String
        """
        payment = self.soap_dict(payment)
        private_data_list = self.soap_dict(private_data_list)
        return self.ws_request('doCapture', 'DirectPaymentAPI',
            transactionID=transaction_id, payment=payment,
            privateDataList=private_data_list, sequenceNumber=sequence_number,
            media=media, version=3)

    def do_credit(self, payment, card, comment=None, order=None, buyer=None,
            owner=None, private_data_list=None, media=None):
        """
        :type payment: Payment
        :type card: Card
        :type comment: String
        :type order: Order
        :type buyer: Buyer
        :type owner: Owner
        :type private_data_list: PrivateDataList
        :type media: String
        """
        payment = self.soap_dict(payment)
        card = self.soap_dict(card)
        order = self.soap_dict(order)
        buyer = self.soap_dict(buyer)
        owner = self.soap_dict(owner)
        private_data_list = self.soap_dict(private_data_list)
        return self.ws_request('doCredit', 'DirectPaymentAPI', payment=payment,
            card=card, comment=comment, order=order, buyer=buyer, owner=owner,
            privateDataList=private_data_list, media=media, version=3)

    def do_debit(self, payment, card, order, authorization, buyer=None,
            owner=None, private_data_list=None, authentication3_d_secure=None,
            media=None):
        """
        :type payment: Payment
        :type card: Card
        :type order: Order
        :type authorization: Authorization
        :type buyer: Buyer
        :type owner: Owner
        :type private_data_list: PrivateDataList
        :type authentication3_d_secure: Authentication3DSecure
        :type media: String
        """
        payment = self.soap_dict(payment)
        card = self.soap_dict(card)
        order = self.soap_dict(order)
        authorization = self.soap_dict(authorization)
        buyer = self.soap_dict(buyer)
        owner = self.soap_dict(owner)
        private_data_list = self.soap_dict(private_data_list)
        authentication3_d_secure = self.soap_dict(authentication3_d_secure)
        return self.ws_request('doDebit', 'DirectPaymentAPI', payment=payment,
            card=card, order=order, authorization=authorization, buyer=buyer,
            owner=owner, privateDataList=private_data_list,
            authentication3DSecure=authentication3_d_secure, media=media,
            version=3)

    def do_immediate_wallet_payment(self, payment, order, wallet_id,
            buyer=None, card_ind=None, cvx=None, private_data_list=None,
            media=None):
        """
        :type payment: Payment
        :type order: Order
        :type wallet_id: String
        :type buyer: Buyer
        :type card_ind: String
        :type cvx: String
        :type private_data_list: PrivateDataList
        :type media: String
        """
        payment = self.soap_dict(payment)
        order = self.soap_dict(order)
        buyer = self.soap_dict(buyer)
        private_data_list = self.soap_dict(private_data_list)
        return self.ws_request('doImmediateWalletPayment', 'DirectPaymentAPI',
            payment=payment, order=order, walletId=wallet_id, buyer=buyer,
            cardInd=card_ind, cvx=cvx, privateDataList=private_data_list,
            media=media, version=3)

    def do_re_authorization(self, transaction_id, payment, order=None,
            private_data_list=None, media=None):
        """
        :type transaction_id: String
        :type payment: Payment
        :type order: Order
        :type private_data_list: PrivateDataList
        :type media: String
        """
        payment = self.soap_dict(payment)
        order = self.soap_dict(order)
        private_data_list = self.soap_dict(private_data_list)
        return self.ws_request('doReAuthorization', 'DirectPaymentAPI',
            transactionID=transaction_id, payment=payment, order=order,
            privateDataList=private_data_list, media=media, version=3)

    def do_recurrent_wallet_payment(self, payment, order_ref, order_date,
            scheduled_date, wallet_id, recurring, card_ind=None,
            private_data_list=None, order=None, media=None):
        """
        :type payment: Payment
        :type order_ref: String
        :type order_date: String
        :type scheduled_date: String
        :type wallet_id: String
        :type recurring: Recurring
        :type card_ind: String
        :type private_data_list: PrivateDataList
        :type order: Order
        :type media: String
        """
        payment = self.soap_dict(payment)
        recurring = self.soap_dict(recurring)
        private_data_list = self.soap_dict(private_data_list)
        order = self.soap_dict(order)
        return self.ws_request('doRecurrentWalletPayment', 'DirectPaymentAPI',
            payment=payment, orderRef=order_ref, orderDate=order_date,
            scheduledDate=scheduled_date, walletId=wallet_id,
            recurring=recurring, cardInd=card_ind,
            privateDataList=private_data_list, order=order, media=media,
            version=3)

    def do_refund(self, transaction_id, payment, comment=None,
            private_data_list=None, sequence_number=None, media=None):
        """
        :type transaction_id: String
        :type payment: Payment
        :type comment: String
        :type private_data_list: PrivateDataList
        :type sequence_number: String
        :type media: String
        """
        payment = self.soap_dict(payment)
        private_data_list = self.soap_dict(private_data_list)
        return self.ws_request('doRefund', 'DirectPaymentAPI',
            transactionID=transaction_id, payment=payment, comment=comment,
            privateDataList=private_data_list, sequenceNumber=sequence_number,
            media=media, version=3)

    def do_reset(self, transaction_id, comment=None, media=None):
        """
        :type transaction_id: String
        :type comment: String
        :type media: String
        """
        return self.ws_request('doReset', 'DirectPaymentAPI',
            transactionID=transaction_id, comment=comment, media=media,
            version=3)

    def do_scheduled_wallet_payment(self, payment, scheduled_date, wallet_id,
            order_ref=None, order_date=None, card_ind=None, order=None,
            private_data_list=None, media=None):
        """
        :type payment: Payment
        :type scheduled_date: String
        :type wallet_id: String
        :type order_ref: String
        :type order_date: String
        :type card_ind: String
        :type order: Order
        :type private_data_list: PrivateDataList
        :type media: String
        """
        payment = self.soap_dict(payment)
        order = self.soap_dict(order)
        private_data_list = self.soap_dict(private_data_list)
        return self.ws_request('doScheduledWalletPayment', 'DirectPaymentAPI',
            payment=payment, scheduledDate=scheduled_date, walletId=wallet_id,
            orderRef=order_ref, orderDate=order_date, cardInd=card_ind,
            order=order, privateDataList=private_data_list, media=media,
            version=3)

    def do_scoring_cheque(self, payment, cheque, order, private_data_list=None,
            media=None):
        """
        :type payment: Payment
        :type cheque: Cheque
        :type order: Order
        :type private_data_list: PrivateDataList
        :type media: String
        """
        payment = self.soap_dict(payment)
        cheque = self.soap_dict(cheque)
        order = self.soap_dict(order)
        private_data_list = self.soap_dict(private_data_list)
        return self.ws_request('doScoringCheque', 'DirectPaymentAPI',
            payment=payment, cheque=cheque, order=order,
            privateDataList=private_data_list, media=media, version=3)

    def enable_wallet(self, contract_number, wallet_id, card_ind=None):
        """
        :type contract_number: String
        :type wallet_id: String
        :type card_ind: String
        """
        return self.ws_request('enableWallet', 'DirectPaymentAPI',
            contractNumber=contract_number, walletId=wallet_id,
            cardInd=card_ind)

    def get_balance(self, card_id, contract_number):
        """
        :type card_id: String
        :type contract_number: String
        """
        return self.ws_request('getBalance', 'DirectPaymentAPI',
            cardID=card_id, contractNumber=contract_number, version=3)

    def get_cards(self, contract_number, wallet_id, card_ind=None):
        """
        :type contract_number: String
        :type wallet_id: String
        :type card_ind: String
        """
        return self.ws_request('getCards', 'DirectPaymentAPI',
            contractNumber=contract_number, walletId=wallet_id,
            cardInd=card_ind)

    def get_encryption_key(self, ):
        """
        """
        return self.ws_request('getEncryptionKey', 'DirectPaymentAPI', )

    def get_merchant_settings(self, ):
        """
        """
        return self.ws_request('getMerchantSettings', 'DirectPaymentAPI',
            version=3)

    def get_payment_record(self, contract_number, payment_record_id):
        """
        :type contract_number: String
        :type payment_record_id: String
        """
        return self.ws_request('getPaymentRecord', 'DirectPaymentAPI',
            contractNumber=contract_number, paymentRecordId=payment_record_id,
            version=3)

    def get_token(self, card_number, contract_number, expiration_date=None):
        """
        :type card_number: String
        :type contract_number: String
        :type expiration_date: String
        """
        return self.ws_request('getToken', 'DirectPaymentAPI',
            cardNumber=card_number, contractNumber=contract_number,
            expirationDate=expiration_date)

    def get_wallet(self, contract_number, wallet_id, card_ind=None,
            media=None):
        """
        :type contract_number: String
        :type wallet_id: String
        :type card_ind: String
        :type media: String
        """
        return self.ws_request('getWallet', 'DirectPaymentAPI',
            contractNumber=contract_number, walletId=wallet_id,
            cardInd=card_ind, media=media, version=3)

    def un_block(self, transaction_id, transaction_date=None):
        """
        :type transaction_id: String
        :type transaction_date: String
        """
        return self.ws_request('unBlock', 'DirectPaymentAPI',
            transactionID=transaction_id, transactionDate=transaction_date,
            version=3)

    def update_wallet(self, contract_number, wallet, card_ind=None, buyer=None,
            owner=None, private_data_list=None, authentication3_d_secure=None,
            media=None, contract_number_wallet_list=None):
        """
        :type contract_number: String
        :type wallet: Wallet
        :type card_ind: String
        :type buyer: Buyer
        :type owner: Owner
        :type private_data_list: PrivateDataList
        :type authentication3_d_secure: Authentication3DSecure
        :type media: String
        :type contract_number_wallet_list: ContractNumberWalletList
        """
        wallet = self.soap_dict(wallet)
        buyer = self.soap_dict(buyer)
        owner = self.soap_dict(owner)
        private_data_list = self.soap_dict(private_data_list)
        authentication3_d_secure = self.soap_dict(authentication3_d_secure)
        contract_number_wallet_list = self.soap_dict(contract_number_wallet_list)
        return self.ws_request('updateWallet', 'DirectPaymentAPI',
            contractNumber=contract_number, wallet=wallet, cardInd=card_ind,
            buyer=buyer, owner=owner, privateDataList=private_data_list,
            authentication3DSecure=authentication3_d_secure, media=media,
            contractNumberWalletList=contract_number_wallet_list, version=3)

    def verify_authentication(self, contract_number, pares, card, md=None):
        """
        :type contract_number: String
        :type pares: String
        :type card: Card
        :type md: String
        """
        card = self.soap_dict(card)
        return self.ws_request('verifyAuthentication', 'DirectPaymentAPI',
            contractNumber=contract_number, pares=pares, card=card, md=md,
            version=3)

    def verify_enrollment(self, card, payment, order_ref, md_field_value=None,
            user_agent=None):
        """
        :type card: Card
        :type payment: Payment
        :type order_ref: String
        :type md_field_value: String
        :type user_agent: String
        """
        card = self.soap_dict(card)
        payment = self.soap_dict(payment)
        return self.ws_request('verifyEnrollment', 'DirectPaymentAPI',
            card=card, payment=payment, orderRef=order_ref,
            mdFieldValue=md_field_value, userAgent=user_agent, version=3)
